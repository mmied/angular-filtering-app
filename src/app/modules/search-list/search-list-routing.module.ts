import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListComponent } from './list/list.component';
import { ItemsListResolver } from './services/items-list.resolver';

const routes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        component: ListComponent,
        resolve: {
            data: ItemsListResolver
        },
        runGuardsAndResolvers: 'paramsOrQueryParamsChange'
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SearchListRoutingModule { }
