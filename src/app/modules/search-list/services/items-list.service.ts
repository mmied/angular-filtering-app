import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { AbstractRestService } from 'src/app/shared/services/abstract-rest.service';
import { ListItem } from 'src/app/shared/types';

@Injectable()
export class ItemsListService extends AbstractRestService<ListItem[]> {
    protected resource = 'items-list';

    constructor(protected http: HttpClient) {
        super();
    }

    protected unwrapResponse(response: ListItem[]) {
        return response;
    }
}
