import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map} from 'rxjs/operators';
import { Observable } from 'rxjs';

import { ListItem } from 'src/app/shared/types';
import { sortFilters, typeFilters } from './filters-options';
import { SearchFilterService } from 'src/app/shared/services/search-filter.service';
import { ListFiltersForm } from './models/list-filters-form';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.sass'],
    providers: [ListFiltersForm]
})
export class ListComponent implements OnInit {
    private listItems$: Observable<ListItem[]>;
    public filteredList$: Observable<ListItem[]>;

    public sortFilters = sortFilters;
    public typeFilters = typeFilters;

    constructor(
        private route: ActivatedRoute,
        public filterService: SearchFilterService,
        private listFilters: ListFiltersForm
    ) {}

    ngOnInit(): void {
        this.listItems$ = this.route.data.pipe(map(res => res.data));

        this.filterService.connectFiltersForm(
            this.listFilters.queryParamsToFormValues,
            this.listFilters.formValueToParams);

        this.filteredList$ = this.filterService.filterData(this.listItems$);
    }
}
