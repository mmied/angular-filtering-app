import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { debounceTime, map } from 'rxjs/operators';
import { Observable, combineLatest } from 'rxjs';

import { ListItem, FilterParams } from '../types';
import { LogicAndFilter, PropertyFilter, NestedPropertyFilter, KeywordFilter, LogicOrFilter } from '../quering/filters';
import { PropertySortClouse } from '../quering/sort-clauses';

@Injectable()
export class SearchFilterService {
    public filtersForm: FormGroup;
    private filterFormDebounceTime = 100;

    constructor(
        private route: ActivatedRoute,
        private router: Router
    ) {}

    public connectFiltersForm(
        queryParamsToFormValues: (params: any) => FormGroup,
        formValueToParams: (formValue: any) => Partial<FilterParams>
    ) {
        this.filtersForm = queryParamsToFormValues(this.route.snapshot.queryParams);
        this.filtersForm.valueChanges
        .pipe(
            debounceTime(this.filterFormDebounceTime),
            map(value => this.onFiltersChange(formValueToParams(value)))
        ).subscribe();
    }

    private onFiltersChange(params: Partial<FilterParams>): void {
        this.changeQueryParams({...params});
    }

    private changeQueryParams(params: Partial<FilterParams>): void {
        this.router
        .navigate(['.'], {
            relativeTo: this.route,
            queryParams: params,
            queryParamsHandling: 'merge'
        });
    }

    public filterData(listItems$: Observable<ListItem[]>): Observable<ListItem[]> {
        const queryParams$ = this.route.queryParams;

        return combineLatest(listItems$, queryParams$).pipe(
            map(([items, queryParams]) => this.filterDataByGivenParams(items, queryParams))
        );
    }

    private filterDataByGivenParams(items: ListItem[], queryParams: any): ListItem[] {
        const filter = new LogicAndFilter([]);

        if (queryParams.query) {
            filter.addFilter(
                new LogicOrFilter([
                    new PropertyFilter('title', queryParams.query),
                    new NestedPropertyFilter('type', 'code', queryParams.query),
                    new KeywordFilter(queryParams.query)
                ])
            );
        }

        if (queryParams.type) {
            filter.addFilter(new NestedPropertyFilter('type', 'code', queryParams.type));
        }

        let filteredItems = filter.filter(items);

        if (queryParams.order) {
            const normalize = (a: any) => a.toLowerCase();
            filteredItems = new PropertySortClouse('title', queryParams.order, normalize).sort(filteredItems);
        }

        return filteredItems;
    }

    public clearFilters(): void {
        this.router
        .navigate(['.'], {
            relativeTo: this.route,
            queryParams: []
        });

        this.filtersForm.reset();
    }
}
