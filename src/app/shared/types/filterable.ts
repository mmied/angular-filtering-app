export type OrderType = 'ascending' | 'descending';

export interface FilterParams {
    query: string;
    type?: string;
    order?: OrderType;
}
