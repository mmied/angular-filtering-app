import { ListItem } from '../types';

abstract class SortClause {
    constructor(protected direction: string, protected normalizator: (a: ListItem) => any = null) {}

    public sort(items: ListItem[]): ListItem[] {
        return items.sort(this.compare);
    }

    protected doCompare(a: any, b: any): number {
        if (a === b) {
            return 0;
        }

        if ('ascending' === this.direction) {
            return a > b ? 1 : -1;
        } else {
            return a < b ? 1 : -1;
        }
    }

    protected normalize(a: ListItem): any {
        return null == this.normalizator ? a : this.normalizator(a);
    }

    protected abstract compare(a: ListItem, b: ListItem): number;
}

export class PropertySortClouse extends SortClause {
    constructor(
        protected property: string,
        protected direction: string,
        protected normalizator: (a: ListItem) => any = null
    ) {
        super(direction, normalizator);
    }

    protected compare = (a: ListItem, b: ListItem): number => {
        const an = this.normalize(a[this.property]) ;
        const bn = this.normalize(b[this.property]);

        return this.doCompare(an, bn);
    }
}