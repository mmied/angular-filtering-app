class Type {
    constructor(code) {
        this.name = code[0].toUpperCase() + code.slice(1);
        this.code = code;
    }
}

const codes = ['video', 'document', 'image', 'audio'];

const types = codes.map(code => new Type(code));

module.exports = types;