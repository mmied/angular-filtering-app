const types = require('./types');
const keywords = require('./keywords');

class ListItem {
    constructor(
        title,
        types,
        keywords
    ) {
        this.id = this.generateUniqueId();
        this.title = title;
        this.type = this.getRandomElement(types, 'array');
        this.keywords = this.getRandomElement(keywords);
    }

    generateUniqueId = () => {
        let dt = new Date().getTime();
        const uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
            const r = (dt + Math.random()*16)%16 | 0;
            dt = Math.floor(dt/16);

            return (c == 'x' ? r : (r&0x3|0x8)).toString(16);
        });
        return uuid;
    }

    getRandomElement(elements, asType) {
        const length = elements.length;

        const getRandomIndex = (min, max) => {
            min = Math.ceil(min);
            max = Math.floor(max);
            return Math.floor(Math.random() * (max - min)) + min;
        }

        const random = getRandomIndex(0, length);

        return asType === 'array' ? elements[random] : [elements[random]];
    }
}

const titles = [
    'Lorem ipsum dolor sit amet',
    'consectetur adipiscing elit',
    'sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veni',
    'quis nostrud exercitation ul ut aliquip ex ea commodo consequat. ',
    'Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.',
    'Excepteur sint occaecat cupidatat non proi',
    'sunt in culpa qui officia de id est laborum.',
    'sed do eiusmod tempor incidi aliqua. Ut enim ad minim veni',
    'Lorem ipsum dolor sit amet',
    'consectetur adipiscing elit',
    'Duis aute irure dolor in rep velit esse cillum dolore eu fugiat nulla pariatur.',
    'quis nostrud exercitation ul ex ea commodo consequat. ',
    'Excepteur sint occaecat cupi',
    'sunt in culpa qui officia deserunt mollit anim id est laborum.',
];
const items = titles.map(title => new ListItem(title, types, keywords));

module.exports = items;