class Keyword {
    constructor(code) {
        this.name = code[0].toUpperCase() + code.slice(1);
        this.code = code;
    }
}

const keywordCodes = ['spielberg', 'metallica', 'music album', 'best practices', 'health', 'programming'];

const keywords = keywordCodes.map(code => new Keyword(code));

module.exports = keywords;