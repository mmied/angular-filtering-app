const getResponse = (collection) => {
    return (req, res) => res.json(collection);
};

module.exports = { getResponse };