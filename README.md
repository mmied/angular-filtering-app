### This a demo filtering app.

### Starting the application
1. Go to project folder and run `npm install`
2. Run API with `npm run start:api`
3. Open new terminal window/tab and run the project with `npm start`

#### Folder structure:

 - core - responsible for collecting app all crucial services and components, like authentication service, route guards, header/footer etc
 - shared - low level utilities, shared single responsibility components, common services, pipes, directives etc.
 - modules - complex components structure with many responsibilities

Application could have different project structure, but the purpose was to show you my usual approach.

##### PS. Testing will be added later on.